﻿// @Credit (https://stackoverflow.com/questions/50409639/check-if-collision-between-the-model-and-two-different-objects-is-happening-at-t/50415096#50415096)

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionDetection : MonoBehaviour {

    // For Bitwise Operations
    public int INDEX = 1;
    public int MIDDLE = 2;
    public int RING = 4;
    public int PINKY = 8;
    public int THUMB = 16;

    // Haptic Feedback Finger Tips Tags
    public const string hapticIndexTag = "hapticIndex";
    public const string hapticMiddleTag = "hapticMiddle";
    public const string hapticRingTag = "hapticRing";
    public const string hapticPinkyTag = "hapticPinky";
    public const string hapticThunbTag = "hapticThumb";

    static public int HapticOutput = 0;
    static private List<KeyValuePair<GameObject, GameObject>> collisionList = new List<KeyValuePair<GameObject, GameObject>>();

    void OnTriggerEnter(Collider other)
    {
        //Debug.Log("Trigger Entered");

        //Get the two Objects involved in the collision
        GameObject col1 = this.gameObject;
        GameObject col2 = other.gameObject;

        //Add to the collison List
        if (col2.name != "LeftHand")
        {
            RegisterEvents(collisionList, col1, col2);
            switch (col1.tag)
            {
                case hapticIndexTag:
                    HapticOutput |= INDEX;
                    break;
                case hapticMiddleTag:
                    HapticOutput |= MIDDLE;
                    break;
                case hapticRingTag:
                    HapticOutput |= RING;
                    break;
                case hapticPinkyTag:
                    HapticOutput |= PINKY;
                    break;
                case hapticThunbTag:
                    HapticOutput |= THUMB;
                    break;
                default:
                    Debug.Log("No Tag Found");
                    break;
            }
        }
    }

    void OnTriggerExit(Collider other)
    {
        //Debug.Log("Trigger Exit");

        //Get the two Objects involved in the collision
        GameObject col1 = this.gameObject;
        GameObject col2 = other.gameObject;

        //Remove from the collison List
        UnRegisterEvents(collisionList, col1, col2);
        switch (col1.tag)
        {
            case hapticIndexTag:
                HapticOutput &= (byte)(~(int)INDEX);
                break;
            case hapticMiddleTag:
                HapticOutput &= (byte)(~(int)MIDDLE);
                break;
            case hapticRingTag:
                HapticOutput &= (byte)(~(int)RING);
                break;
            case hapticPinkyTag:
                HapticOutput &= (byte)(~(int)PINKY);
                break;
            case hapticThunbTag:
                HapticOutput &= (byte)(~(int)THUMB);
                break;
            default:
                Debug.Log("No Tag Found");
                break;
        }
    }

    public static bool IsTouching(GameObject obj1, GameObject obj2)
    {
        int matchIndex = 0;
        return _itemExist(collisionList, obj1, obj2, ref matchIndex);
    }

    private void UnRegisterEvents(List<KeyValuePair<GameObject, GameObject>> existingObj, GameObject col1, GameObject col2)
    {
        int matchIndex = 0;

        //Remove if it exist
        if (_itemExist(existingObj, col1, col2, ref matchIndex))

        {
            existingObj.RemoveAt(matchIndex);
        }
    }

    private void RegisterEvents(List<KeyValuePair<GameObject, GameObject>> existingObj, GameObject col1, GameObject col2)
    {
        int matchIndex = 0;

        //Add if it doesn't exist
        if (!_itemExist(existingObj, col1, col2, ref matchIndex))

        {
            KeyValuePair<GameObject, GameObject> item = new KeyValuePair<GameObject, GameObject>(col1, col2);
            existingObj.Add(item);
        }
    }

    private static bool _itemExist(List<KeyValuePair<GameObject, GameObject>> existingObj, GameObject col1,
    GameObject col2, ref int matchIndex)
    {
        bool existInList = false;
        for (int i = 0; i < existingObj.Count; i++)
        {
            //Check if key and value exist and vice versa
            if ((existingObj[i].Key == col1 && existingObj[i].Value == col2) ||
                    (existingObj[i].Key == col2 && existingObj[i].Value == col1))
            {
                existInList = true;
                matchIndex = i;
                break;
            }
        }
        return existInList;
    }
}
